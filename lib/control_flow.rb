# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  answer = ""
  str.each_char do |ch|
    if ch == ch.downcase
      next
    else
      answer += ch
    end
  end
  answer
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  float_mid = str.length.to_f / 2
  if float_mid == float_mid.to_i
    str[float_mid.to_i - 1] + str[float_mid.to_i]
  else
    str[float_mid.to_i]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  realv = "aeiouAEIOU"
  str.count(realv)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  answer = ""
  counter = 0
  arr.each do |el|
    answer += el
    counter += 1
    if counter < arr.length
      answer += separator
    end
  end
  answer
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  counter = 1
  answer = ""
  str.each_char do |ch|
    if counter.odd?
      answer += ch.downcase
      counter += 1
    else
      answer += ch.upcase
      counter += 1
    end
  end
  answer
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(" ")
  arr.map! do |wrd|
    if wrd.length >= 5
      wrd.reverse
    else
      wrd
    end
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map! do |num|
    if num % 3 == 0 && num % 5 == 0
      "fizzbuzz"
    elsif num % 3 == 0
      "fizz"
    elsif num % 5 == 0
      "buzz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  answer = []
  arr.each { |el| answer.unshift(el) }
  answer
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2...num).to_a.each do |n|
    if num % n == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = (1..num).to_a
  arr.map! do |el|
    if num % el == 0
      el
    end
  end
  arr.compact
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  factors.map! do |el|
    if prime?(el)
      el
    end
  end
  factors.compact
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  oddstuff = []
  evenstuff = []
  arr.each do |el|
    if el.even?
      evenstuff << el
    else
      oddstuff << el
    end
  end
  if oddstuff.length == 1
    oddstuff[0]
  else
    evenstuff[0]
  end
end
